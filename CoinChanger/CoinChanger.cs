﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class CoinChanger
    {

        static void Main(string[] args)
        {
            double quantity = 11.78;
            Console.Write("Which coins should I give to sum up {0} ?\n", quantity);
            printCoins(GetChange(quantity));
            Console.ReadLine();
        }

        internal static Dictionary<double,int> GetChange(double quantity)
        {
            Dictionary<double, int> change = new Dictionary<double, int>();

            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 0);
            
            List<double> coins = new List<double>();
            coins.Add(2.00);
            coins.Add(1.00);
            coins.Add(0.50);
            coins.Add(0.20);
            coins.Add(0.10);
            coins.Add(0.05);
            coins.Add(0.02);
            coins.Add(0.01);

            foreach (double coin in coins)
            {
                if (quantity / coin >= 1)
                {
                    change[coin] = (int) (quantity / coin);
                    quantity = Math.Round(quantity % coin,2);
                    if (quantity == 0.0)
                        break;
                }
            }

            return change;
        }

        internal static void printCoins(Dictionary<double,int> coins)
        {
            foreach (KeyValuePair<double, int> pair in coins)
            {
                if (pair.Value != 0)
                    if (pair.Value == 1)
                        Console.WriteLine("{0} coin of {1}", pair.Value, pair.Key);
                    else
                        Console.WriteLine("{0} coins of {1}", pair.Value, pair.Key);
            }
        }
    }
}
