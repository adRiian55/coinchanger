﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    [TestFixture]
    public class CoinChangerTest
    {
        
        [Test]
        public void ChangeOfOneCoinIsTheSameCoin([Values(0.01,0.02,0.05,0.10,0.20,0.50,1.00,2.00)] double quantity)
        {
            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 0);

            change[quantity] = 1;

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));

        }
        [Test]
        public void FourIsTwoCoinsOfTwo()
        {
            double quantity = 4.00;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 2);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void SixIsThreeCoinsOfTwo()
        {
            double quantity = 6.00;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 3);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void EightIsForCoinsOfTwo()
        {
            double quantity = 8.00;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 4);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void FourtyCentsIsTwoCoinsOfTwentyCents()
        {
            double quantity = 0.40;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 2);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 0);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void FourCentsIsTwoCoinsOfTwoCents()
        {
            double quantity = 0.04;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 2);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 0);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void ThirtyCentsIsOneCoinOfTwentyAndOneOfTen()
        {
            double quantity = 0.30;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 1);
            change.Add(0.20, 1);
            change.Add(0.50, 0);
            change.Add(1.00, 0);
            change.Add(2.00, 0);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void FourPointFiveIsOneCoinOfTwentyCentsAndOneOfTwo()
        {
            double quantity = 4.50;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 0);
            change.Add(0.05, 0);
            change.Add(0.10, 0);
            change.Add(0.20, 0);
            change.Add(0.50, 1);
            change.Add(1.00, 0);
            change.Add(2.00, 2);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }

        [Test]
        public void ThreeDot67Is1Of2And1of1And1Of50And1Of10And1Of5And1Of2()
        {
            double quantity = 3.67;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 0);
            change.Add(0.02, 1);
            change.Add(0.05, 1);
            change.Add(0.10, 1);
            change.Add(0.20, 0);
            change.Add(0.50, 1);
            change.Add(1.00, 1);
            change.Add(2.00, 1);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }


        [Test]
        public void ElevenDot78Is5Of2And1of1And1Of50And1Of20And1Of5And1Of2And1Of1()
        {
            double quantity = 11.78;

            Dictionary<double, int> change = new Dictionary<double, int>();
            change.Add(0.01, 1);
            change.Add(0.02, 1);
            change.Add(0.05, 1);
            change.Add(0.10, 0);
            change.Add(0.20, 1);
            change.Add(0.50, 1);
            change.Add(1.00, 1);
            change.Add(2.00, 5);

            Assert.AreEqual(change, CoinChanger.GetChange(quantity));
        }
    }
}
